<?php

namespace Drupal\phpwarnings\Plugin\Block;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Database\Connection;
use Drupal\Core\Link;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\Markup;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Lists PHP warnings and errors.
 *
 * @Block(
 *   id = "phpwarnings",
 *   admin_label = @Translation("PHP warnings & errors"),
 *   category = @Translation("Devel")
 * )
 */
class PhpWarnings extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The current database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected Connection $db;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('database')
    );
  }

  /**
   * Constructs the block object.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param string $plugin_id
   *   The plugin ID.
   * @param array $plugin_definition
   *   The plugin definition.
   * @param \Drupal\Core\Database\Connection $connection
   *   The current database connection.
   */
  public function __construct(
    array $configuration,
          $plugin_id,
          $plugin_definition,
    Connection $connection
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->db = $connection;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $statuses = ['emergency', 'alert', 'critical', 'error', 'warning', 'notice', 'info', 'debug'];

    $result = $this->db->select('watchdog', 'w')
      ->fields('w', ['wid', 'severity', 'message', 'variables'])
      ->condition('type', 'php')
      ->orderBy('timestamp', 'DESC')
      ->execute();

    $items = [];
    $unique_results = [];

    foreach ($result as $row) {
      $vars = @unserialize($row->variables);
      if (!is_array($vars)) {
        $vars = [];
      }
      $message = strtr($row->message, $vars);
      if (($i = strpos($message, '#0 ')) > 0) {
        $message = substr($message, 0, $i);
      }

      if (in_array($message, $unique_results)) {
        continue;
      }
      $unique_results[] = $message;

      $message = Markup::create($message);
      $url = Url::fromRoute('dblog.event', ['event_id' => $row->wid]);

      $items[] = [
        '#wrapper_attributes' => [
          'class' => ['status-' . $statuses[$row->severity]],
        ],
        "#children" => Link::fromTextAndUrl($message, $url)->toString(),
      ];
    }

    // Do not display the block if it contains no items.
    if (empty($items)) {
      return [
        '#markup' => 'No log messages available.',
        '#cache' => ['max-age' => 0],
      ];
    }

    // Format it as a nice list.
    return [
      '#theme' => 'item_list',
      '#list_type' => 'ul',
      '#items' => $items,
      '#attached' => ['library' => ['phpwarnings/phpwarnings']],
      '#cache' => ['max-age' => 0],
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    return AccessResult::allowedIf($account->hasPermission('access site reports'));
  }

}
